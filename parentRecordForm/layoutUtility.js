// Function returns an array of fields formatted into the appropriate number of columns for the the corresponding section template.
const setLayoutArray = (columns, fieldsArray) => {
  let layoutArray = [];
  if (columns <= 1) {
    columns = 1;
  } else if (columns >= 4) {
    columns = 4;
  } else if (columns > 1 && columns < 4) {
  } else {
    columns = 1;
  }
  for (let i = 0; i < fieldsArray.length; i = i + columns) {
    let item = {};
    if (columns === 1) {
      if (fieldsArray[i]) {
        item.one = {
          name: fieldsArray[i].name,
          value: fieldsArray[i].value
        };
        layoutArray.push(item);
      }
    } else if (columns === 2) {
      if (fieldsArray[i]) {
        item.one = {
          name: fieldsArray[i].name,
          value: fieldsArray[i].value
        };
      }
      if (fieldsArray[i + 1]) {
        item.two = {
          name: fieldsArray[i + 1].name,
          value: fieldsArray[i + 1].value
        };
      }
      layoutArray.push(item);
    } else if (columns === 3) {
      if (fieldsArray[i]) {
        item.one = {
          name: fieldsArray[i].name,
          value: fieldsArray[i].value
        };
      }
      if (fieldsArray[i + 1]) {
        item.two = {
          name: fieldsArray[i + 1].name,
          value: fieldsArray[i + 1].value
        };
      }
      if (fieldsArray[i + 2]) {
        item.three = {
          name: fieldsArray[i + 2].name,
          value: fieldsArray[i + 2].value
        };
      }
      layoutArray.push(item);
    } else {
      if (fieldsArray[i]) {
        item.one = {
          name: fieldsArray[i].name,
          value: fieldsArray[i].value
        };
      }
      if (fieldsArray[i + 1]) {
        item.two = {
          name: fieldsArray[i + 1].name,
          value: fieldsArray[i + 1].value
        };
      }
      if (fieldsArray[i + 2]) {
        item.three = {
          name: fieldsArray[i + 2].name,
          value: fieldsArray[i + 2].value
        };
      }
      if (fieldsArray[i + 3]) {
        item.four = {
          name: fieldsArray[i + 3].name,
          value: fieldsArray[i + 3].value
        };
      }
      layoutArray.push(item);
    }
  }
  return layoutArray;
};
export { setLayoutArray };
