import { LightningElement, api, track, wire } from "lwc";
import { setLayoutArray } from "./layoutUtility";

export default class ParentRecordForm extends LightningElement {
  @api object = "Account";
  @api recordId = "";
  @api recordTypeId = "";
  @track sectionOneColumns = 3;
  @track sectionTwoColumns = 2;
  @track sectionOneClassName =
    "slds-col slds-size_1-of-" + this.sectionOneColumns;
  @track sectionTwoClassName =
    "slds-col slds-size_1-of-" + this.sectionTwoColumns;
  @track variant = "label-stacked";
  @track required = false;
  // section set up
  @track sectionOne = [
    { name: "Name", value: this.Name },
    { name: "Industry", value: this.Industry },
    { name: "Phone", value: this.Phone },
    { name: "Type", value: this.Type },
    { name: "AccountNumber", value: this.AccountNumber },
    { name: "AnnualRevenue", value: this.AnnualRevenue },
    { name: "NumberOfEmployees", value: this.NumberOfEmployees },
    { name: "Fax", value: this.Fax },
    { name: "Custom_Date__c", value: this.Custom_Date__c },
    { name: "Custom_Number__c", value: this.Custom_Number__c },
    { name: "Custom_Text__c", value: this.Custom_Text__c }
  ];
  // duplicate of the same fields but used for demo purposes to show multiple sections.
  @track sectionTwo = [
    { name: "Name", value: this.Name },
    { name: "Industry", value: this.Industry },
    { name: "Phone", value: this.Phone },
    { name: "Type", value: this.Type },
    { name: "AccountNumber", value: this.AccountNumber },
    { name: "AnnualRevenue", value: this.AnnualRevenue },
    { name: "NumberOfEmployees", value: this.NumberOfEmployees },
    { name: "Fax", value: this.Fax },
    { name: "Custom_Date__c", value: this.Custom_Date__c },
    { name: "Custom_Number__c", value: this.Custom_Number__c },
    { name: "Custom_Text__c", value: this.Custom_Text__c }
  ];
  // layout assignments
  @track layoutArrayOne = setLayoutArray(
    this.sectionOneColumns,
    this.sectionOne
  );
  @track layoutArrayTwo = setLayoutArray(4, this.sectionTwo);
  // field value tracking
  @track Name;
  @track Industry;
  @track Phone;
  @track Type;
  @track AccountNumber;
  @track AnnualRevenue;
  @track NumberOfEmployees;
  @track Fax;
  @track Custom_Date__c;
  @track Custom_Number__c;
  @track Custom_Text__c;

  handleOnChange(event) {
    this[event.target.fieldName] = event.target.value;
    console.log("field name: " + event.target.fieldName);
    console.log("value: " + event.target.value);
    console.log(JSON.stringify(this.layoutArrayOne));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.template.querySelectorAll("lightning-record-edit-form")[0].submit();
  }

  handleOnSuccess(event) {
    console.log("success: " + event.detail.id);
    this.recordId = event.detail.id;
  }

  handleError(event) {
    console.log("ERRORS : " + JSON.stringify(event));
  }
}
