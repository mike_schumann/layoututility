# README

Layout Utility - Version 1

The Problem - Creating custom record edit forms require a large amount of html to create. The reason is the shadow dom. For a lightning input field to pick up the object api name for reference it needs to be nested inside the record edit form lightning component. If we were to try and dynamically render lightning input fields inside a custom component the shadow boundary restricts the lightning input field from picking up the object api name.

Due to this restriction the solution I've come up with is create a layout utility. The layout utitlity takes 2 parameters. Columns and an array of the fields and values you want to pass into section template. Keeping mobile design in mind I've limited the number of columns to 4 so the fields aren't too compressed on the page. If an invalid value for columns is entered it defaults to 1.

Setup -

I have included an example for reference called the parentRecordForm component in this repostitory. The component is an example of set up and usage. The component includes the utitlity javascript file as well as the section template as its own html file. However some of the key set up points are listed below.

You will need to add the layout utility somewhere you can reference it for your import statement. Then you will need to import it into your component:

import { setLayoutArray } from "./layoutUtility";

Once imported you will need to set up your fields for the layout utility. The fields array can take any number of fields passed in the following structure:
@track fieldsToBeSentToLayoutUtility = [ { name: "Name", value: this.Name } ];

@api would also work but it should be an attribute so you can pass values into the sectionFields to be handled by your onchange handler.

The fields to be passed to your template can be set up like the following:
@track sectionFieldsToBeSentToTemplate = setLayoutArray(this.columns, this.fieldsToBeSentToLayoutUtility);

Section Template -

The sectionTemplate.html is just for reference for how you need to set up the html for each sectionArray in your layout. So it will render the fields you have sent to the layout utility. The field-name and value attributes are required attributes on the lightning input fields used in the template. All other attributes of the lightning input field can be customized for your purposes.

Each section in your form would have its own set of fields sent to the layout utility and each formatted array would have its own section template used to create the form. By setting it up this way you can easiliy switch the layout and formatting of your form and cut down the hundreds of lines of html it typically takes to create a custom record edit form.
